function imageClick(e) {
    var fi = document.getElementById("full-image-img");
    var href = e.target.parentElement.href;
    if (fi.src != href) {
        fi.src = href;
        fi.parentElement.classList.remove("hidden");
    } else {
        fi.parentElement.classList.add("hidden");
        fi.src = "";
        console.log("Hidden");
    }
    e.preventDefault();
}

var element = document.getElementById("gallery");
element.classList.add("container");

var imageDiv = document.createElement("div");
imageDiv.id = "full-image-div";
imageDiv.classList.add("full-image", "center");
var image = document.createElement("img");
image.id = "full-image-img";
imageDiv.appendChild(image);

element.appendChild(imageDiv);

for (var i = 1; i <= 4; i++) {
    var row = document.createElement("div");
    row.classList.add("row");
    for (var j = 0; j < 5; j++) {
        var num = i * 4 + j;
        var a = document.createElement("a");
        a.href = "img/gallery/fullsize/" + num + ".jpg";

        var img = document.createElement("img");
        img.src = "img/gallery/preview/" + num + ".jpg";
        img.classList.add("gallery-img");

        a.onclick = imageClick;

        a.appendChild(img);
        row.appendChild(a);
    }
    element.appendChild(row);
}